# %% Libraries
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy import stats
from sklearn.metrics import roc_curve, roc_auc_score
from sklearn.metrics import confusion_matrix, classification_report, precision_score
from sklearn import metrics
from sklearn.ensemble import RandomForestClassifier
from sklearn.feature_selection import SelectFromModel
import shap
from sklearn.model_selection import train_test_split
from xgboost import XGBRegressor, XGBClassifier, plot_importance, plot_tree
from processing.general import normalize_string_value
import matplotlib.pylab as pl
import copy
from sklearn import tree
import math

# %% Read Data
data = pd.read_csv("./modeling/v5_new_tech_score/final_altdata_merged_ft.csv", dtype={"personId": str})

# %% create mapping sheet
# mapping_sheet = pd.DataFrame({"field": data.columns})
# mapping_sheet.to_excel("./modeling/v5_new_tech_score/ms_pr_altscore+_v5.xlsx", index=False)

# %% Data Cleaning
data_full = data.copy()
data_full.rename(columns={"marca": "default"}, inplace=True)
data_full["default"] = np.where(data_full["default"] == "M", 1, 0)
data_full = data_full[~data_full["default"].isna()].copy()

# drop 1251 obs from 2020
data_full["year"] = data_full["FECHA_OBSERVACION"].str.split("-").str[0]
data_full = data_full[data_full["year"] != "2020"].copy()
data_full.drop(columns=["year"
                        ], inplace=True)
target_variable = "default"

# %% rename var
data_full.rename(columns={"shareThis_count_mac_os_pct": "geoHexData_mac_os_pct"}, inplace=True)

# %% correct special values of hasAccount vars
# emailVerified == 0 and email_disabled == 0, then hasAccount replace with -98
has_account_list = ["emailFootprint_hasAccount_amazon",
                    "emailFootprint_hasAccount_facebook",
                    "emailFootprint_hasAccount_instagram",
                    "emailFootprint_hasAccount_netflix",
                    "emailFootprint_hasAccount_netflix_inactive"]
for c in has_account_list:
    data_full.loc[(data_full["emailFootprint_emailVerify_verified"] == 0) &
                  (data_full["emailFootprint_emailVerify_disabled"] == 0), c] = -999998

data_full.loc[data_full["emailFootprint_hasAccount_netflix"] == -999999, "emailFootprint_hasAccount_netflix"] = -999998
data_full.loc[data_full["emailFootprint_hasAccount_netflix_inactive"] == -999999, "emailFootprint_hasAccount_netflix_inactive"] = -999998

# %% custom variable netflix
cond_list = [(data_full["emailFootprint_hasAccount_netflix_inactive"].isin(["-999999", "-999998", "-999997", -999999, -999998, -999997])),
             (data_full["emailFootprint_hasAccount_netflix_inactive"] == 1),
             (data_full["emailFootprint_hasAccount_netflix_inactive"] == 0) & (data_full["emailFootprint_hasAccount_netflix"] == 1),
             (data_full["emailFootprint_hasAccount_netflix_inactive"] == 0) & (data_full["emailFootprint_hasAccount_netflix"] == 0)
             ]

choice_list = ["99x", "inactive", "active", "no_account"]

data_full["netflix"] = np.select(cond_list, choice_list)

# %% Check score gini in train and test sets to choose best random state
# tmp_l = []
#
# for r_s in range(100):
#     X_train_raw, X_test_raw, y_train_raw, y_test_raw = \
#         train_test_split(data_full[[e for e in data_full.columns if e != target_variable]],
#                          data_full[target_variable], test_size=0.15, random_state=r_s)
#     test_auc = roc_auc_score(y_test_raw, X_test_raw["SCORE"])
#     ps_test = np.abs((2 * test_auc) - 1)
#     score_test_set = X_test_raw[["SCORE"]].copy()
#     score_test_set["default"] = y_test_raw
#     train_auc = roc_auc_score(y_train_raw, X_train_raw["SCORE"])
#     ps_train = np.abs((2 * train_auc) - 1)
#     print(r_s)
#     print(ps_train, ps_test, ps_train-ps_test)
#     print(y_train_raw.mean(), y_test_raw.mean(), y_train_raw.mean() - y_test_raw.mean())
#     print()
#     tmp_row = [r_s, ps_train, ps_test, abs(ps_train - ps_test),
#             y_train_raw.mean(), y_test_raw.mean(), abs(y_train_raw.mean() - y_test_raw.mean())]
#     tmp_l.append(tmp_row)
#
# tmp = pd.DataFrame(tmp_l, columns=["r_s",
#                                    "ps_train",
#                                    "ps_test",
#                                    "diff_ps",
#                                    "train_def",
#                                    "test_def",
#                                    "diff_def"])
# %% split data with best random state, to use at the end for compare scores
random_state_0 = 65
X_train_raw, X_test_raw, y_train_raw, y_test_raw = \
    train_test_split(data_full[[e for e in data_full.columns if e != target_variable]],
                     data_full[target_variable], test_size=0.15, random_state=random_state_0)
print(y_train_raw.mean(), y_test_raw.mean(), abs(y_train_raw.mean() - y_test_raw.mean()))

# %% Variable Cleaning Geo
data_clean = data_full.copy()
mapping_sheet = pd.read_excel("./modeling/v5_new_tech_score/ms_pr_sin_score_v5.xlsx")

# Categorical variables to str
cat_vars = mapping_sheet[mapping_sheet["is_str"] == 1]["field"].to_list()
for c in cat_vars:
    # err_99 = data_clean[c].isin(["-999999", -999999]).sum()
    # if err_99 > 0:
    #     print(c, "-99", err_99)
    data_clean[c] = data_clean[c].apply(normalize_string_value)
    data_clean[c] = data_clean[c].astype(str)

    # treat -99 and -98 as -97
    data_clean[c] = data_clean[c].replace(["-999998", "-999999"], "-999997")

# Ignore variables
ignore_vars = mapping_sheet[mapping_sheet['ignore'] == 1]["field"].to_list()
ignore_vars = [e for e in ignore_vars if e in data_clean]
data_clean.drop(columns=ignore_vars, inplace=True)

# drop columns with 1 unique value
unique_value_cols = [c for c in data_clean.columns if data_clean[c].nunique() == 1]
data_clean.drop(columns=unique_value_cols, inplace=True)

cat_vars = [c for c in cat_vars if c in data_clean.columns]
data_cat_vars = pd.get_dummies(data_clean[cat_vars], drop_first=True)
data_clean.drop(columns=cat_vars, inplace=True)
data_clean = data_clean.join(data_cat_vars)
data_clean.drop(columns=["SCORE"], inplace=True)

# %% Split train-test
target_variable = "default"
X_train, X_test, y_train, y_test = \
    train_test_split(data_clean[[e for e in data_clean.columns if e != target_variable]],
                     data_clean[target_variable], test_size=0.15, random_state=random_state_0)
# y_train2 = (y_train * 2) - 1

# %% Normalize train
# Numerical dataset
num_vars_train = X_train.drop(columns=data_cat_vars.columns).copy()
cat_vars_train = X_train[data_cat_vars.columns].copy()

# Replace with other values
num_vars_train = num_vars_train.replace([-999997, -999998, -999999], np.nan)
num_vars_train = num_vars_train.replace('', np.nan)
num_vars_train = num_vars_train.replace('inf', np.nan)

# Add 'clean' variables (take original but assign mean value to missing) and create indicator of missing values
# Normalize numeric variables from 0-1
norm_list = []
for c in num_vars_train.columns:
    # print(c)
    num_vars_train[c + '_clean'] = num_vars_train[c].fillna(num_vars_train[c].mean())
    num_vars_train[c + '_clean'] = (num_vars_train[c + '_clean'] - min(num_vars_train[c + '_clean'])) / (max(num_vars_train[c + '_clean']) - min(num_vars_train[c + '_clean']))
    num_vars_train[c + '_missing'] = (num_vars_train[c].apply(lambda x: pd.isnull(x))).astype(int)

    norm_col = [c, num_vars_train[c].mean(), num_vars_train[c].min(), num_vars_train[c].max()]
    norm_list.append(norm_col)

    del num_vars_train[c]

norm_params = pd.DataFrame(columns=["variable", "mean", "min", "max"], data=norm_list)
# norm_params.to_csv("./modeling/v3/data_fill_values_v3.csv", index=False)

# Final modeling dataset
X_train_clean = num_vars_train.join(cat_vars_train)

# %% Normalize test set
cat_vars_test = X_test[data_cat_vars.columns].copy()
num_vars_test = X_test.drop(columns=data_cat_vars.columns).copy()

# Replace with other values
num_vars_test = num_vars_test.replace([-999997, -999998, -999999], np.nan)
num_vars_test = num_vars_test.replace('', np.nan)
num_vars_test = num_vars_test.replace('inf', np.nan)

# Add 'clean' variables (take original but assign mean value to missing) and create indicator of missing values
# Normalize numeric variables from 0-1
for c in num_vars_test.columns:
    # print(c)
    num_vars_test[c + '_clean'] = num_vars_test[c].fillna(norm_params[norm_params["variable"] == c]["mean"].values[0])
    num_vars_test[c + '_clean'] = (num_vars_test[c + '_clean'] - norm_params[norm_params["variable"] == c]["min"].values[0]) / \
                                  (norm_params[norm_params["variable"] == c]["max"].values[0] - norm_params[norm_params["variable"] == c]["min"].values[0])
    num_vars_test[c + '_missing'] = (num_vars_test[c].apply(lambda x: pd.isnull(x))).astype(int)
    del num_vars_test[c]

num_vars_test.dtypes.value_counts()
num_vars_test[num_vars_test > 1] = 1
num_vars_test[num_vars_test < 0] = 0

# Final modeling dataset
X_test_clean = num_vars_test.join(cat_vars_test)

# %% Feature Preselection
# rf = RandomForestClassifier(n_estimators=50, random_state=100)
#
# rf.fit(X_train_clean, y_train)
# rf_pred = rf.predict(X_test_clean)
# rf_pred_prob = rf.predict_proba(X_test_clean)
#
# sel = SelectFromModel(rf, prefit=True, threshold=0.001)  # .0008
#
# # See which features are important
# sel.get_support()
# selected_features = X_train_clean.columns[sel.get_support()].tolist()

selected_features = [
    "SaldoCuentaAhorros1_clean",
    "M_DVEN_36_SFR_clean",
    "COPEN_VIG_12_SFR_clean",
    "publicData_educacionSuperior_tituloRegCount_clean",
    "MOLD_SFR_O_clean",
    "geoHexData_mac_os_pct_clean",
    "in_sbs_cant_oper_dpd6e_1",
    "ncon_ret_1_12_clean",
    "MMONTO_VIG_3_TC_clean",
    "geoHexData_building_res9_count_adm2PctOfMax_clean",
    "M_SAL_ODIF36_3_clean",
    "publicData_electricidadCNELNoParam_planillasPendientesCount_clean",
    "geoHexData_building_res8_meanArea_kn1PctOfMax_clean",
    "MOLD_SICOM_O_clean",
    "emailFootprint_emailVerify_verified_1.0",
    "emailFootprint_hasAccount_amazon_1.0",
    "publicData_educacionSecundaria_diasDesdeGrado_clean",
    "geoHexData_idx_costOfLiving_v1_adm1_clean",
    "personSearch_emailBreach_daysSinceLast_clean",
    "emailFootprint_emailVerify_disabled_1.0",
    "netflix_active",

    "geoHexData_road_res8_count_residential_adm0PctOfMax_clean",
    "geoHexData_idx_techAndConnectivity_v1_adm0_clean",
    "publicData_electricidadCNEL_contratosCount_clean",
    "Provincia_pichincha",
    "personSearch_emailBreach_daysSinceFirst_clean"
]

selected_features = [
    "SaldoCuentaAhorros1_clean",
    "M_DVEN_36_SFR_clean",
    "COPEN_VIG_12_SFR_clean",
    "publicData_educacionSuperior_tituloRegCount_clean",
    "MOLD_SFR_O_clean",
    "geoHexData_mac_os_pct_clean",
    "in_sbs_cant_oper_dpd6e_1",
    "ncon_ret_1_12_clean",
    "MMONTO_VIG_3_TC_clean",
    # "geoHexData_building_res9_count_adm2PctOfMax_clean",
    "M_SAL_ODIF36_3_clean",
    "publicData_electricidadCNELNoParam_planillasPendientesCount_clean",
    # "geoHexData_building_res8_meanArea_kn1PctOfMax_clean",
    "MOLD_SICOM_O_clean",
    "emailFootprint_emailVerify_verified_1.0",
    "emailFootprint_hasAccount_amazon_1.0",
    "personSearch_emailBreach_daysSinceLast_clean",
    # "emailFootprint_emailVerify_disabled_1.0",
    "netflix_active",

    "geoHexData_road_res8_count_residential_adm0PctOfMax_clean",
    "geoHexData_idx_techAndConnectivity_v1_adm0_clean",
    "publicData_electricidadCNEL_contratosCount_clean",
    "Provincia_pichincha",
]

X_train_new = X_train_clean[selected_features].copy()
X_test_new = X_test_clean[selected_features].copy()

df_list = [X_train_new, X_test_new]
for i, df in enumerate(df_list, 1):
    df.columns = [col_name[:-6] if "_clean" in col_name else col_name for col_name in df.columns]

train_complete = X_train_new.copy()
train_complete[f"{target_variable}"] = y_train

# %% Split train set in validation-set
X_train_val, X_val, y_train_val, y_val = \
    train_test_split(train_complete[[e for e in train_complete.columns if e != target_variable]],
                     train_complete[target_variable], test_size=0.10, random_state=1)
print(y_train_val.mean(), y_val.mean(), abs(y_train_val.mean() - y_val.mean()))

# %% XG Boost Classifier Hyperparam tuning by hand
# initial values
# n_estimators = [50, 75, 100, 150, 300]
# learning_rates = [0.01, 0.05, 0.1, 0.3]
# gamma = [1, 5, 10, 15]
# cs_tree = [0.8, 1]
# min_child = [0.8, 1]
# max_depth = [1, 2, 3]

# n_estimators = [80, 90, 100, 110, 120]
# learning_rates = [0.08, 0.1, 0.12, 0.15]
# gamma = [1, 8, 10, 12]
# cs_tree = [0.8, 1]
# min_child = [0.8, 1]
# max_depth = [2, 3]

n_estimators = [70, 75, 80, 90, 95, 100]
learning_rates = [0.2, 0.25, 0.3, 0.35, 0.4]
gamma = [5, 6, 7, 8, 9, 10]
cs_tree = [0.6, 0.8, 1]
min_child = [0.6, 0.8, 1]
max_depth = [2]

roc_auc_results = []

for estimator in n_estimators:
    for md in max_depth:
        for lr in learning_rates:
            for g in gamma:
                for cs in cs_tree:
                    for mcw in min_child:
                        xg_boost = XGBClassifier(
                            n_estimators=estimator, learning_rate=lr,
                            max_depth=md, random_state=100, subsample=mcw, colsample_bytree=cs,
                            min_child_weight=1, gamma=g, use_label_encoder=False
                        )
                        # xg_boost.fit(X_train_val, y_train_val)
                        #
                        # test_xg_boost_pred_prob = xg_boost.predict_proba(X_val)[:, 1]
                        # roc_auc_test = roc_auc_score(y_val, test_xg_boost_pred_prob)
                        #
                        # train_xg_boost_pred_prob = xg_boost.predict_proba(X_train_val)[:, 1]
                        # roc_auc_train = roc_auc_score(y_train_val, train_xg_boost_pred_prob)
                        #
                        # scored_test = pd.DataFrame()
                        # scored_test["1"] = test_xg_boost_pred_prob
                        # scored_test[f"{target_variable}"] = y_val.reset_index()[f"{target_variable}"]
                        # ks_test = stats.ks_2samp(scored_test[scored_test[target_variable] == 0]["1"],
                        #                          scored_test[scored_test[target_variable] == 1]["1"]).statistic
                        #
                        # scored_train = pd.DataFrame()
                        # scored_train["1"] = train_xg_boost_pred_prob
                        # scored_train[f"{target_variable}"] = y_train_val.reset_index()[f"{target_variable}"]
                        # ks_train = stats.ks_2samp(scored_train[scored_train[target_variable] == 0]["1"],
                        #                           scored_train[scored_train[target_variable] == 1]["1"]).statistic
                        #
                        # gini_test = (roc_auc_test * 2) - 1
                        # gini_train = (roc_auc_train * 2) - 1

                        xg_boost.fit(X_train_new, y_train)
                        xg_boost_test_prob = xg_boost.predict_proba(X_test_new)[:, 1]
                        test_f = (roc_auc_score(y_test, xg_boost_test_prob) * 2) - 1

                        xg_boost_train_prob = xg_boost.predict_proba(X_train_new)[:, 1]
                        train_f = (roc_auc_score(y_train, xg_boost_train_prob) * 2) - 1

                        combination = [estimator, lr, g, cs, md, mcw,
                                       # gini_test, gini_train, ks_train, ks_test,
                                       train_f, test_f]
                        print(combination)
                        roc_auc_results.append(combination)

z_cl = pd.DataFrame(roc_auc_results, columns=["n_estimators",
                                              "learning_rate",
                                              "g",
                                              "cs_tree",
                                              "md",
                                              "mcw",
                                              # "gini_test",
                                              # "gini_train",
                                              # "ks_train",
                                              # "ks_test",
                                              "train_f",
                                              "test_f"])
# # z_cl.to_excel("./modeling/v3/pt_001.xlsx", index=False)

# %% Classifier
xg_boost = XGBClassifier(
    # 26
    # n_estimators=80, learning_rate=0.15,
    # max_depth=2, random_state=100, subsample=0.8, colsample_bytree=0.8,
    # gamma=8

    # best no amazon (20 vars)
    n_estimators=70, learning_rate=0.35,
    max_depth=2, random_state=100, subsample=0.6, colsample_bytree=0.8,
    gamma=8

    # n_estimators=100, learning_rate=0.35,
    # max_depth=2, random_state=100, subsample=0.6, colsample_bytree=0.8,
    # gamma=6

)
xg_boost.fit(X_train_new, y_train)
xg_boost_test_prob = xg_boost.predict_proba(X_test_new)[:, 1]
print((roc_auc_score(y_test, xg_boost_test_prob) * 2) - 1)

xg_boost_train_prob = xg_boost.predict_proba(X_train_new)[:, 1]
print((roc_auc_score(y_train, xg_boost_train_prob) * 2) - 1)

scored_test = pd.DataFrame()
scored_test["1"] = xg_boost_test_prob
# scored_test.reset_index(inplace=True)
scored_test[f"{target_variable}"] = y_test.reset_index()[f"{target_variable}"]
ks = stats.ks_2samp(scored_test[scored_test[target_variable] == 0]["1"],
                    scored_test[scored_test[target_variable] == 1]["1"]).statistic
print(ks)

scored_train = pd.DataFrame()
scored_train["1"] = xg_boost_train_prob
# scored_train.reset_index(inplace=True)
scored_train[f"{target_variable}"] = y_train.reset_index()[f"{target_variable}"]
ks = stats.ks_2samp(scored_train[scored_train[target_variable] == 0]["1"],
                    scored_train[scored_train[target_variable] == 1]["1"]).statistic
print(ks)

imp = xg_boost.feature_importances_
imp_df = pd.DataFrame({"feature_name": list(selected_features),
                       "importance": imp})
print(imp_df[imp_df["importance"] != 0].count())

X_total = pd.concat([X_train_new, X_test_new])
X_total.reset_index(inplace=True, drop=True)
y_total = pd.concat([y_train, y_test])
y_total.reset_index(inplace=True, drop=True)

# %%
#
#
# def lim_func(m):
#     """
#     Limit size of regressor classifier
#     """
#     return float(max(min(m, 1), -1))
#
#
# def get_predictions(model, data_set):
#     """
#     Compute model predictions on data_set
#     output: pred and pred_prob
#     """
#     # Make Predictions on Test Set
#     model_pred_prob = model.predict(data_set)
#     model_pred_prob = pd.DataFrame(model_pred_prob, columns=['1'])
#     model_pred_prob['1'] = np.vectorize(lim_func)(model_pred_prob)
#
#     # Transform to binary (1, 0)
#     model_sign = np.sign(model_pred_prob)
#     model_pred = (model_sign + 1) / 2
#
#     # Transform to prob
#     model_pred_prob = (model_pred_prob + 1) / 2
#
#     return model_pred, model_pred_prob
#
#
# def get_metrics(y, pred_prob):
#     """
#     Compute general metrics
#     """
#     return roc_auc_score(y, pred_prob)
#
#
# #%%
# n_estimators = [125, 150]
# learning_rates = [0.08, 0.1, 0.12, 0.15]
# gamma = [0, 1, 3]
# cs_tree = [0.8, 1]
# min_child = [0, 0.1]
#
#
# roc_auc_results = []
#
# for estimator in n_estimators:
#     for lr in learning_rates:
#         for g in gamma:
#             for cs in cs_tree:
#                 for mcw in min_child:
#                     xg_boost = XGBRegressor(
#                         n_estimators=estimator, learning_rate=lr,
#                         max_depth=1, random_state=100, subsample=cs, colsample_bytree=1,
#                         min_child_weight=mcw, gamma=g
#                     )
#                     xg_boost.fit(X_train_new, y_train2)
#
#                     test_xg_boost_pred, test_xg_boost_pred_prob = get_predictions(xg_boost, X_test_new)
#                     roc_auc_test = get_metrics(y_test, test_xg_boost_pred_prob)
#
#                     train_xg_boost_pred, train_xg_boost_pred_prob = get_predictions(xg_boost, X_train_new)
#                     roc_auc_train = get_metrics(y_train, train_xg_boost_pred_prob)
#
#                     gini_test = (roc_auc_test * 2) - 1
#                     gini_train = (roc_auc_train * 2) - 1
#
#                     combination = [estimator, lr, g, cs, mcw, gini_test, gini_train]
#                     print(combination)
#                     roc_auc_results.append(combination)
#
# z = pd.DataFrame(roc_auc_results, columns=["n_estimators",
#                                            "learning_rate",
#                                            "g",
#                                            "cs_tree",
#                                            "mcw",
#                                            "gini_test",
#                                            "gini_train"
#                                            ])
#
# # %%
# xg_boost = XGBRegressor(
#     n_estimators=150, learning_rate=0.05,
#     max_depth=2, random_state=100, subsample=0.8, colsample_bytree=1,
#     min_child_weight=0, gamma=7
# )
# xg_boost.fit(X_train_new, y_train2)
# xg_boost_test, xg_boost_test_prob = get_predictions(xg_boost, X_test_new)
# print((get_metrics(y_test, xg_boost_test_prob) * 2) - 1)
#
# xg_boost_train, xg_boost_train_prob = get_predictions(xg_boost, X_train_new)
# print((get_metrics(y_train, xg_boost_train_prob) * 2) - 1)

# %% Plot All Shapley values
explainer = shap.TreeExplainer(xg_boost, X_total)
shap_values = explainer(X_total)
# X_total.head().to_csv("test_develop_shap.csv", index=False)

shap.plots.bar(copy.deepcopy(shap_values), show=False, max_display=35)
fig = plt.gcf()
fig.set_size_inches(15.5, 10.5)
plt.tight_layout()
plt.show()

shap.plots.beeswarm(copy.deepcopy(shap_values), max_display=35, show=False)
fig = plt.gcf()
fig.set_size_inches(18.5, 10.5)
plt.tight_layout()
plt.show()

# %% custom shapley plots
shap.summary_plot(shap_values, X_total, plot_type="bar", show=False, plot_size=(15, 8),
                  feature_names=[
                      "publicData_educacionSuperior_tituloRegExtranjeroPct_missing",
                      "EdadPtoObservación",
                      "geoHexData_mac_os_pct",
                      "publicData_electricidadCNELNoParam_planillasPendientesCount",
                      "publicData_educacionSuperior_tituloRegCount",
                      "geoHexData_idx_techAndConnectivity_v1_adm0",
                      "geoHexData_road_res8_count_residential_adm0PctOfMax",
                      "Provincia_pichincha",
                      "emailFootprint_emailVerify_verified_1.0",
                      "emailFootprint_hasAccount_amazon_1.0",
                      "netflix_active",
                      "publicData_electricidadCNEL_contratosCount",
                      "personSearch_emailBreach_daysSinceFirst",
                  ]
                  )
fig = plt.gcf()
fig.set_size_inches(15.5, 10.5)
plt.tight_layout()
plt.show()

shap.plots.beeswarm(copy.deepcopy(shap_values), order=[4, 2, 11, 10, 7, 3,
                                                       1, 5, 0, 6,
                                                       8, 12, 9],
                    max_display=15, show=False)
fig = plt.gcf()
fig.set_size_inches(18.5, 10.5)
plt.tight_layout()
plt.show()

# shap.plots.scatter(shap_values[:, 363], show=False)
# fig = plt.gcf()
# fig.set_size_inches(13.5, 10.5)
# plt.tight_layout()
# plt.show()
#
# shap.plots.scatter(shap_values[:, shap_values.abs.mean(0).values.argsort()[-1]],
#                    color=shap_values[:, shap_values.abs.mean(0).values.argsort()[-6]],
#                    show=False)
# fig = plt.gcf()
# fig.set_size_inches(13.5, 10.5)
# plt.tight_layout()
# plt.show()
#
# shap.plots.waterfall(shap_values[9339], show=False)
# fig = plt.gcf()
# fig.set_size_inches(14.5, 10.5)
# plt.tight_layout()
# plt.show()
#
# shap.plots.waterfall(shap_values[19430], show=False, max_display=9)
# fig = plt.gcf()
# fig.set_size_inches(14.5, 10.5)
# plt.tight_layout()
# plt.show()

# %% trees from variables
d_tree = tree.DecisionTreeClassifier(max_depth=1)
d_tree = d_tree.fit(data_full["shareThis_count_ios_pct"].values.reshape(-1, 1),
                    data_full["default"])
tree.plot_tree(d_tree, proportion=True)
plt.show()

data_full.groupby("geoHexData_mac_os_pct").agg(['mean', 'count'])["default"]

# %%
l = [
    "emailFootprint_hasAccount_amazon",
    #  "emailFootprint_hasAccount_facebook",
    #  "emailFootprint_hasAccount_instagram",
    # "emailFootprint_hasAccount_netflix",
    # "emailFootprint_hasAccount_netflix_inactive",
    "netflix"
]

tmp = data_full.fillna(0).copy()
for i in l:
    print(tmp.groupby(i)['default'].agg(['count', 'mean']))
    print()

# %% Compare with existing model whole dataset
probs = xg_boost.predict_proba(X_total)[:, 1]

comp_data_modeling = pd.concat([X_train_raw, X_test_raw])
comp_data_modeling.reset_index(inplace=True, drop=True)
comp_data_modeling["default"] = y_total
comp_data_modeling["probability"] = probs
# comp_data_modeling['old_score_bucket'] = pd.qcut(comp_data_modeling['SCORE'], q=5)
# comp_data_modeling['new_score_bucket'] = pd.qcut(comp_data_modeling['new_score'], q=5)
# print(comp_data_modeling.groupby("old_score_bucket").mean()["default"])
# print(comp_data_modeling.groupby("new_score_bucket").mean()["default"])
# agg_scores = comp_data_modeling.groupby(['old_score_bucket', 'new_score_bucket'])['default'].agg(['count', 'mean'])
# print(agg_scores)

# agg_scores.reset_index(inplace=True)
# agg_scores = agg_scores.merge(comp_data_modeling.groupby("old_score_bucket").mean()["default"],
#                               on="old_score_bucket")
# agg_scores.to_excel("./modeling/v4/altscore+.xlsx", index=False)

# %% get only features with importance (shapleys no 0)
# for i in range(1347):
#     if np.unique(shap_values[:, i].values)[0] != 0:
#         print(shap_values.feature_names[i])

# %%
print((roc_auc_score(comp_data_modeling[comp_data_modeling["emailFootprint_emailVerify_verified"] == 1]["default"],
                     comp_data_modeling[comp_data_modeling["emailFootprint_emailVerify_verified"] == 1]["new_score"]) * 2) - 1)
print((roc_auc_score(comp_data_modeling[comp_data_modeling["emailFootprint_emailVerify_verified"] != 1]["default"],
                     comp_data_modeling[comp_data_modeling["emailFootprint_emailVerify_verified"] != 1]["new_score"]) * 2) - 1)


# %% change to score


def points_func(value, old_max, old_min, new_max, new_min):
    return ((value - old_min) * (new_max - new_min) / (old_max - old_min)) + new_min


def map_points(value):
    """
    Transform probs (0-1000) score
    """
    if 0.0886 <= value <= 1:
        return points_func(value, 0.0886, 1, 100, 0)
    elif 0.0555 <= value < 0.0886:
        return points_func(value, 0.0555, 0.0886, 300, 100)
    elif 0.0356 <= value < 0.0555:
        return points_func(value, 0.0356, 0.0555, 500, 300)
    elif 0.0207 <= value < 0.0356:
        return points_func(value, 0.0207, 0.0356, 700, 500)
    elif 0 <= value < 0.0207:
        return points_func(value, 0, 0.0207, 1000, 700)


comp_data_modeling["altscore"] = comp_data_modeling.apply(
    lambda row:
    map_points(row["probability"]),
    axis=1)

# %% compute gini and ks with altscore
print("prob gini:", (roc_auc_score(comp_data_modeling[target_variable], comp_data_modeling["new_score"]) * 2) - 1)
print("prob ks:", stats.ks_2samp(comp_data_modeling[comp_data_modeling[target_variable] == 0]["new_score"],
                                 comp_data_modeling[comp_data_modeling[target_variable] == 1]["new_score"]).statistic)

print("altscore gini:", (roc_auc_score(comp_data_modeling[target_variable], comp_data_modeling["altscore"]) * 2) - 1)
print("altscore ks:", stats.ks_2samp(comp_data_modeling[comp_data_modeling[target_variable] == 0]["altscore"],
                                     comp_data_modeling[comp_data_modeling[target_variable] == 1]["altscore"]).statistic)

# %% ratings of score
comp_data_modeling['altscore_bucket'] = pd.qcut(comp_data_modeling['altscore'], q=10)
print(comp_data_modeling.groupby("altscore_bucket").agg(["mean", "count"])[target_variable])

plt.figure(figsize=(25, 10))
d_tree = tree.DecisionTreeClassifier(max_depth=4)
d_tree = d_tree.fit(comp_data_modeling["altscore"].values.reshape(-1, 1),
                    comp_data_modeling["default"])
tree.plot_tree(d_tree, proportion=True)
plt.show()

# %% manual ratings from trees
cond_list = [
    (comp_data_modeling["altscore"] > 825) & (comp_data_modeling["altscore"] <= 1000),
    (comp_data_modeling["altscore"] > 708) & (comp_data_modeling["altscore"] <= 825),
    (comp_data_modeling["altscore"] > 630) & (comp_data_modeling["altscore"] <= 708),
    (comp_data_modeling["altscore"] > 478) & (comp_data_modeling["altscore"] <= 630),
    (comp_data_modeling["altscore"] > 361) & (comp_data_modeling["altscore"] <= 478),
    (comp_data_modeling["altscore"] > 280) & (comp_data_modeling["altscore"] <= 361),
    (comp_data_modeling["altscore"] > 200) & (comp_data_modeling["altscore"] <= 280),
    (comp_data_modeling["altscore"] > 98) & (comp_data_modeling["altscore"] <= 200),
    (comp_data_modeling["altscore"] > 94) & (comp_data_modeling["altscore"] <= 98),
    (comp_data_modeling["altscore"] > 0) & (comp_data_modeling["altscore"] <= 94),
]

choice_list = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"]

comp_data_modeling["ratings"] = np.select(cond_list, choice_list)

ratings_tree = comp_data_modeling.groupby(["ratings"]).agg(["mean", "count"])[target_variable]
ratings_tree["pct_pob"] = ratings_tree["count"] / 21412
print(ratings_tree.sort_values("mean").to_markdown())

# %%
# comp_data_modeling[["probability", "altscore", "ratings"]].to_csv("./modeling/v5_new_tech_score/final_scripts/scored_dev_completo.csv", index=False)

# %% both scores decision tree
comp_data_modeling['altscore_bucket'] = pd.qcut(comp_data_modeling['altscore'], q=5)

plt.figure(figsize=(25, 10))
d_tree = tree.DecisionTreeClassifier(max_depth=4)
d_tree = d_tree.fit(comp_data_modeling[["SCORE", "altscore"]],
                    comp_data_modeling["default"])
tree.plot_tree(d_tree,
               proportion=True,
               )
# plt.show()
plt.savefig('./modeling/v4/tree_high_dpi.png', dpi=100)

# %%
var = "geoHexData_idx_techAndConnectivity_v1_adm0"
tmp = data_full[data_full[var] != -999997][[var, target_variable]].copy()
tmp["ind"] = 1
tmp['var_bucket'] = pd.qcut(tmp['geoHexData_idx_techAndConnectivity_v1_adm0'], q=5)
pd.crosstab(tmp.var_bucket, tmp.ind, values=tmp[target_variable], aggfunc='mean').round(3)

# %% selected num vars analysis
num_selected_features = [
    "M_DVEN_36_SFR"
]
tmp_num_var = num_selected_features[0]
print(tmp_num_var)

# has data vs errors
num_analysis_0 = data_full.copy()
# data_clean[c] = data_clean[c].replace(["-999998", "-999999"], "-999997")
num_analysis_0.loc[~num_analysis_0[tmp_num_var].isin([-999997, -999998, -999999]), tmp_num_var] = "data"
print(num_analysis_0.groupby(tmp_num_var).agg(['mean', 'count'])[target_variable])
print()

# buckets not considering errors
num_analysis_1 = data_full[~data_full[tmp_num_var].isin([-999997, -999998, -999999])][[tmp_num_var, target_variable]].copy()
num_analysis_1["ind"] = 1
num_analysis_1['var_bucket'] = pd.qcut(num_analysis_1[tmp_num_var], q=2)
# print(num_analysis_1.groupby(tmp_num_var).agg(['mean', 'count'])[target_variable])
print(pd.crosstab(num_analysis_1["var_bucket"], num_analysis_1["ind"], values=num_analysis_1[target_variable], aggfunc='mean').round(3))
print()

# buckets considering errors
num_analysis_2 = data_full.copy()
num_analysis_2["ind"] = 1
num_analysis_2['var_bucket'] = pd.qcut(num_analysis_2[tmp_num_var], q=2)
print(pd.crosstab(num_analysis_2["var_bucket"], num_analysis_2["ind"], values=num_analysis_2[target_variable], aggfunc='mean').round(3))

# %% selected cat vars analysis
cat_selected_features = [
    '_missing',
    'emailFootprint_emailVerify_verified',
    'emailFootprint_hasAccount_amazon',
    'Provincia',
    'netflix',
]

tmp_cat_var = cat_selected_features[4]
print(tmp_cat_var)

# variable before treating special values
cat_analysis_0 = data_full.copy()
cat_analysis_0[tmp_cat_var] = cat_analysis_0[tmp_cat_var].fillna("NaN")
print(cat_analysis_0.groupby(tmp_cat_var).agg(['mean', 'count'])[target_variable])
print()

# %% 330 cases not geo info (default rate 5.45%)
data_full[data_full["geoHexData_lon"] == -999997][[target_variable]].mean()






























# %%
selected_features_raw = [
    'SaldoCuentaAhorros1',
    'M_DVEN_36_SFR',
    'COPEN_VIG_12_SFR',
    'publicData_educacionSuperior_tituloRegCount',
    'MOLD_SFR_O',
    'geoHexData_mac_os_pct',
    'in_sbs_cant_oper_dpd6e',
    'ncon_ret_1_12',
    'MMONTO_VIG_3_TC',
    'M_SAL_ODIF36_3',
    'publicData_electricidadCNELNoParam_planillasPendientesCount',
    'MOLD_SICOM_O',
    'emailFootprint_emailVerify_verified',
    'emailFootprint_hasAccount_amazon',
    'personSearch_emailBreach_daysSinceLast',
    "emailFootprint_hasAccount_netflix",
    "emailFootprint_hasAccount_netflix_inactive",
    'geoHexData_road_res8_count_residential_adm0PctOfMax',
    'geoHexData_idx_techAndConnectivity_v1_adm0',
    'publicData_electricidadCNEL_contratosCount',
    'Provincia'
]

# %% test produ
data_validation = pd.read_csv("./modeling/v5_new_tech_score/final_altdata_merged_ft_validation_new.csv", dtype={"personId": str})
data_to_score = data_validation.copy()
data_to_score.rename(columns={
    "emailFootprint_breach_count": "personSearch_emailBreach_count",
    "emailFootprint_breach_count01Y": "personSearch_emailBreach_count01Y",
    "emailFootprint_breach_count03Y": "personSearch_emailBreach_count03Y",
    "emailFootprint_breach_count05Y": "personSearch_emailBreach_count05Y",
    "emailFootprint_breach_emailAddressesCount": "personSearch_emailBreach_emailAddressesCount",
    "emailFootprint_breach_namesCount": "personSearch_emailBreach_namesCount",
    "emailFootprint_breach_passwordsCount": "personSearch_emailBreach_passwordsCount",
    "emailFootprint_breach_ipAddressesCount": "personSearch_emailBreach_ipAddressesCount",
    "emailFootprint_breach_creditCardsCount": "personSearch_emailBreach_creditCardsCount",
    "emailFootprint_breach_phoneNumbersCount": "personSearch_emailBreach_phoneNumbersCount",
    "emailFootprint_breach_linkedin": "personSearch_emailBreach_linkedin",
    "emailFootprint_breach_myspace": "personSearch_emailBreach_myspace",
    "emailFootprint_breach_canva": "personSearch_emailBreach_canva",
    "emailFootprint_breach_chegg": "personSearch_emailBreach_chegg",
    "emailFootprint_breach_forbes": "personSearch_emailBreach_forbes",
    "emailFootprint_breach_cannabiscom": "personSearch_emailBreach_cannabiscom",
    "emailFootprint_breach_datacamp": "personSearch_emailBreach_datacamp",
    "emailFootprint_breach_daysSinceLast": "personSearch_emailBreach_daysSinceLast",
    "emailFootprint_breach_daysSinceFirst": "personSearch_emailBreach_daysSinceFirst",
    "geoHexData_idx_techAndConnectivity_v1_adm0_y": "geoHexData_idx_techAndConnectivity_v1_adm0",
    "shareThis_count_mac_os_pct": "geoHexData_mac_os_pct"
}, inplace=True)

# %% data verification
data_to_score["email_y"].isna().sum()  # 234
data_to_score["email_x"].isna().sum()  # 229

data_to_score["emailFootprint_emailVerify_verified"].value_counts()  # 1 - 8531, 0 - 1846
data_to_score["geoHexData_lat"].isna().sum()  # 157
data_to_score["geoHexData_lat"].value_counts()  # -97 - 97

data_to_score = data_to_score[selected_features_raw].copy()

for c in data_to_score.columns:
    nans = data_to_score[c].isna().sum()
    if nans > 0:
        print(c, nans)
# geoHexData_road_res8_count_residential_adm0PctOfMax 157 - nan geoHexData_lat
# geoHexData_idx_techAndConnectivity_v1_adm0 589
# geoHexData_mac_os_pct 3555
# emailFootprint_emailVerify_verified 229 - nan email_x
# emailFootprint_hasAccount_amazon 229 - email_x
# emailFootprint_hasAccount_netflix 229 - email_x
# emailFootprint_hasAccount_netflix_inactive 229 - email_x
# personSearch_emailBreach_daysSinceLast 229 - email_x

for c in data_to_score.columns:
    error_99 = (data_to_score[c].isin([-999997, "-999997"])).sum()
    if error_99 > 0:
        print(c, error_99)
# geoHexData_road_res8_count_residential_adm0PctOfMax 429
# publicData_electricidadCNELNoParam_planillasPendientesCount 8267 - -97 en contratosCount
# personSearch_emailBreach_daysSinceFirst 5626 - 0 personSearch_emailBreach_count

print()

# %% data cleaning
# emailVerified == 0, then hasAccount replace with -98
has_account_list = ["emailFootprint_hasAccount_amazon",
                    "emailFootprint_hasAccount_netflix",
                    "emailFootprint_hasAccount_netflix_inactive"]
for c in has_account_list:
    data_to_score[c] = data_to_score[c].fillna("-999997")
    data_to_score.loc[data_to_score["emailFootprint_emailVerify_verified"] == 0, c] = -999998

data_to_score.loc[data_to_score["emailFootprint_hasAccount_netflix"] == -999999, "emailFootprint_hasAccount_netflix"] = -999998
data_to_score.loc[data_to_score["emailFootprint_hasAccount_netflix_inactive"] == -999999, "emailFootprint_hasAccount_netflix_inactive"] = -999998

# %% custom variable netflix
cond_list = [(data_to_score["emailFootprint_hasAccount_netflix_inactive"].isin(["-999999", "-999998", "-999997", -999999, -999998, -999997])),
             (data_to_score["emailFootprint_hasAccount_netflix_inactive"] == 1),
             (data_to_score["emailFootprint_hasAccount_netflix_inactive"] == 0) & (data_to_score["emailFootprint_hasAccount_netflix"] == 1),
             (data_to_score["emailFootprint_hasAccount_netflix_inactive"] == 0) & (data_to_score["emailFootprint_hasAccount_netflix"] == 0)
             ]

choice_list = ["error99", "inactive", "active", "no_account"]

data_to_score["netflix"] = np.select(cond_list, choice_list)

# %% fix issue profile vars
data_to_score["emailFootprint_hasAccount_amazon"] = data_to_score["emailFootprint_hasAccount_amazon"].astype(float)

# %% Variable Cleaning Geo
data_to_score_clean = data_to_score.copy()

# Categorical variables to str
cat_vars = mapping_sheet[mapping_sheet["is_str"] == 1]["field"].to_list()
cat_vars = [cat_var for cat_var in data_to_score if cat_var in cat_vars]
for c in cat_vars:
    data_to_score_clean[c] = data_to_score_clean[c].fillna("-999997")
    data_to_score_clean[c] = data_to_score_clean[c].apply(normalize_string_value)
    data_to_score_clean[c] = data_to_score_clean[c].astype(str)
    # print(c)

    # treat -99 and -98 as -97
    data_to_score_clean[c] = data_to_score_clean[c].replace(["-999998", "-999999"], "-999997")

# Ignore variables
ignore_vars = mapping_sheet[mapping_sheet['ignore'] == 1]["field"].to_list()
ignore_vars = [e for e in ignore_vars if e in data_to_score_clean]
data_to_score_clean.drop(columns=ignore_vars, inplace=True)

# drop columns with 1 unique value
unique_value_cols = [c for c in data_to_score_clean.columns if data_to_score_clean[c].nunique() == 1]
data_to_score_clean.drop(columns=unique_value_cols, inplace=True)
data_to_score_clean_0 = data_to_score_clean.copy()

cat_vars = [c for c in cat_vars if c in data_to_score_clean.columns]
data_cat_vars = pd.get_dummies(data_to_score_clean[cat_vars], drop_first=False)
data_to_score_clean.drop(columns=cat_vars, inplace=True)
data_to_score_clean = data_to_score_clean.join(data_cat_vars)

# %% Normalize test set
X_to_score = data_to_score_clean.copy()

cat_vars_to_score = X_to_score[data_cat_vars.columns].copy()
num_vars_to_score = X_to_score.drop(columns=data_cat_vars.columns).copy()

# Replace with other values
num_vars_to_score = num_vars_to_score.replace([-999997, -999998, -999999], np.nan)
num_vars_to_score = num_vars_to_score.replace('', np.nan)
num_vars_to_score = num_vars_to_score.replace('inf', np.nan)

# Add 'clean' variables (take original but assign mean value to missing) and create indicator of missing values
# Normalize numeric variables from 0-1
for c in num_vars_to_score.columns:
    # print(c)
    num_vars_to_score[c + '_clean'] = num_vars_to_score[c].fillna(norm_params[norm_params["variable"] == c]["mean"].values[0])
    num_vars_to_score[c + '_clean'] = (num_vars_to_score[c + '_clean'] - norm_params[norm_params["variable"] == c]["min"].values[0]) / \
                                      (norm_params[norm_params["variable"] == c]["max"].values[0] - norm_params[norm_params["variable"] == c]["min"].values[0])
    num_vars_to_score[c + '_missing'] = (num_vars_to_score[c].apply(lambda x: pd.isnull(x))).astype(int)
    del num_vars_to_score[c]

num_vars_to_score[num_vars_to_score > 1] = 1
num_vars_to_score[num_vars_to_score < 0] = 0

# Final modeling dataset
X_to_score_clean = num_vars_to_score.join(cat_vars_to_score)

X_to_score_new = X_to_score_clean[selected_features].copy()

df_list = [X_to_score_new]
for i, df in enumerate(df_list, 1):
    df.columns = [col_name[:-6] if "_clean" in col_name else col_name for col_name in df.columns]

# %%
probs = xg_boost.predict_proba(X_to_score_new)[:, 1]

# %%
scored_validation = X_to_score_new.copy()
scored_validation["probability"] = probs
scored_validation["altscore"] = scored_validation.apply(
    lambda row:
    map_points(row["probability"]),
    axis=1)

cond_list = [
    (scored_validation["altscore"] > 825) & (scored_validation["altscore"] <= 1000),
    (scored_validation["altscore"] > 708) & (scored_validation["altscore"] <= 825),
    (scored_validation["altscore"] > 630) & (scored_validation["altscore"] <= 708),
    (scored_validation["altscore"] > 478) & (scored_validation["altscore"] <= 630),
    (scored_validation["altscore"] > 361) & (scored_validation["altscore"] <= 478),
    (scored_validation["altscore"] > 280) & (scored_validation["altscore"] <= 361),
    (scored_validation["altscore"] > 200) & (scored_validation["altscore"] <= 280),
    (scored_validation["altscore"] > 98) & (scored_validation["altscore"] <= 200),
    (scored_validation["altscore"] > 94) & (scored_validation["altscore"] <= 98),
    (scored_validation["altscore"] > 0) & (scored_validation["altscore"] <= 94),
]

choice_list = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"]

scored_validation["ratings"] = np.select(cond_list, choice_list)
# scored_validation[["probability", "altscore", "ratings"]].to_csv("./modeling/v5_new_tech_score/final_scripts/scored_test_completo.csv", index=False)
scored_validation = pd.concat([data_validation[["personId", "FECHA_OBSERVACION"]],
                           scored_validation], axis=1)
# scored_validation.to_csv("./modeling/v5_new_tech_score/final_scripts/pr_scored_test_completo_vars.csv", index=False)

# %%
var = "personSearch_emailBreach_daysSinceLast"
var = "emailFootprint_breach_daysSinceLast"
# a = data_to_score[var]
# b = data_full[var]
a = data_to_score[~data_to_score[var].isin([-999997, -999998, -999999,
                                            "-999997", "-999998", "-999999"])][var]
b = data_full[~data_full[var].isin([-999997, -999998, -999999,
                                    "-999997", "-999998", "-999999"])][var]
fig, axes = plt.subplots(nrows=1, ncols=2, figsize=(15, 10))
axes[0].hist(a)
axes[1].hist(b)
plt.show()

# %%
a = data_to_score[~data_to_score[var].isin([-999997, -999998, -999999,
                                            "-999997", "-999998", "-999999"])][var]
b = data_full[~data_full[var].isin([-999997, -999998, -999999,
                                    "-999997", "-999998", "-999999"])][var]
fig, axes = plt.subplots(nrows=1, ncols=2, figsize=(15, 10))
axes[0].hist(a)
axes[1].hist(b)
plt.show()

print(a.describe(), b.describe())

# %%
data_to_score[var].value_counts() / len(data_to_score)
data_full[var].value_counts() / len(data_full)

# %% Gini por variable
vars_buró = [
    "MOLD_SFR_O",
    "MOLD_IN_O",
    "MOLD_SFNR_O",
    "MOLD_SICOM_O",
    "MMONTO_VIG_3_TC",
    "M_SAL_ODIF36_3",
    "ncon_ret_1_12",
    "COPEN_VIG_12_SFR",
    "M_DVEN_36_SFR",
    "SaldoCuentaAhorros1",
    "SaldoCuentaCorrientes1",
]
for i in vars_buró:
    test_auc = roc_auc_score(data_full[target_variable], data_full[i])
    print(i, np.abs((2 * test_auc) - 1))

# %%
plt.figure(figsize=(25, 10))
d_tree = tree.DecisionTreeClassifier(max_depth=3)
d_tree = d_tree.fit(data_full[["M_DVEN_36_SFR", "MOLD_SFR_O"]],
                    data_full["default"])
tree.plot_tree(d_tree,
               proportion=True,
               )
plt.show()

# %% duplication
scored_validation["probs_vars"] = d_tree.predict_proba(scored_validation[["M_DVEN_36_SFR", "MOLD_SFR_O"]])[:, 1]

expanded_db = pd.DataFrame()

for i in range(len(scored_validation)):
    tmp_ones = pd.DataFrame()
    tmp_ones["prob"] = [scored_validation.loc[i, "probability"]] * int(round(scored_validation.loc[i, "probs_vars"] * 1000, 0))
    tmp_ones["dec_tree_vars"] = [1] * int(round(scored_validation.loc[i, "probs_vars"] * 1000, 0))

    tmp_ceros = pd.DataFrame()
    tmp_ceros["prob"] = [scored_validation.loc[i, "probability"]] * (1000 - int(round(scored_validation.loc[i, "probs_vars"] * 1000, 0)))
    tmp_ceros["dec_tree_vars"] = [0] * (1000 - int(round(scored_validation.loc[i, "probs_vars"] * 1000, 0)))

    expanded_db = pd.concat([expanded_db, tmp_ceros, tmp_ones], ignore_index=True)
